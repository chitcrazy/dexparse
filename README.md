这是一个解析dex的项目，在其中，以dex后缀名结尾的都是APK中解压出来的dex文件，专门用来做测试；.c文件就是程序的主要核心部分，主要以ReadDEX.c为主，.sh文件时一个脚本文件，能够直接把ReadDex.c编译成一个共享的动态链接库，DEXDump.c中有main执行入口，编译成DEXDump可执行文件，每次只需要改变ReadDex.c中的解析Dex文件的代码，然后重新编译成共享库文件即可，最后运行程序的命令如下：
./DEXDump xxx.dex
代码暂时写的很粗糙，后期还有很多要改进的

详细编译流程如下：
1.gcc -o ReadDEX.so -std=c99 -fPIC -shared ReadDEX.c  ==>生成动态链接库文件ReadDEX.so
2.gcc -o DEXDump DEXDump.c ./ReadDEX.so  ==>生成可执行主文件DEXDump
3../DEXDump xxx.dex  ==>运行程序